const express = require('express');
const router = express.Router();
const catchAsync = require("../utils/catchAsync");
const PCollection = require("../models/PCollection");
const Product = require("../models/product");

router.get('/', catchAsync(async (req, res) => {
  const PCollections = await PCollection.find({});
  const PCollectionsFiltered = [];

  // removing the products from each PCollections
  for (let collection in PCollections)
  {
    PCollectionsFiltered.push({
      name: PCollections[collection].name,
      image: PCollections[collection].image
    });
  }

  res.send({
    PCollectionsFiltered
  });
}));

router.get('/:categoryName', catchAsync(async (req, res) => {
  const collection = await PCollection.findOne({name: req.params.categoryName});

  if (collection)
  {
    let products = [];
    for (let productId of collection.products)
    {
      let p = await Product.findById(productId);
      products.push(p);
    }
    res.send(products);
  }
  else
  {
    res.send({});
  }

}));


module.exports = router;
