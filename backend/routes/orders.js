const express = require('express');
const axios = require('axios');
const router = express.Router();
const catchAsync = require("../utils/catchAsync");
const Product = require("../models/product");
const Order = require("../models/order");
const {firstStageSchema} = require("../schemas.js");

const EXTERNAL_CLEARING_SERVICE = "http://localhost:9999";

const validateOrderFirstStage = (req, res, next) => {
    {
        const { error } = firstStageSchema.validate(req.body);
        if (error) {
            const msg = error.details.map(el => el.message).join(',')
            return res.send({_id: null, error: true, payload: msg});
        } else {
            next();
        }
    }
}

// personal information
router.post('/firstStage', validateOrderFirstStage, catchAsync(async (req, res) => {
    let validProducts = 0;
    let price = 0;

    // validate each product
    for (const product of req.body.firstStage.products)
    {
        if (await Product.exists({_id: product._id}))
        {
            validProducts++;
            const prod = await Product.findById(product._id);
            price += prod.price * product.amount;
        }
        else
        {
            break;
        }
    }

    if (validProducts)
    {
        const newOrder = await new Order({
            products: req.body.firstStage.products.map(product => product._id),
            firstName: req.body.firstStage.firstName,
            lastName: req.body.firstStage.lastName,
            email: req.body.firstStage.email,
            phoneNumber: req.body.firstStage.phoneNumber,
            street: req.body.firstStage.street,
            optionalStreet: req.body.firstStage.optionalStreet,
            city: req.body.firstStage.city,
            zipcode: req.body.firstStage.zipcode,
            status: false,
            price: price
        });
        await newOrder.save();
        return res.send({_id: newOrder._id, error: false, price: newOrder.price});
    }
    return res.send({_id: null, error: true, payload: "One (or more) of the products doesn't exist in the system"});

}));

// transaction information
router.post('/secondStage', catchAsync(async (req, res) => {
    console.log(req.body);
    const order = await Order.findById(req.body.data._id);
    if (order)
    {
        axios.post(`${EXTERNAL_CLEARING_SERVICE}/validate`,
            {billingInfo: {
                    _id: req.body.data.transactionId,
                    lastName: req.body.data.lastName,
                    lastDigits: req.body.data.lastDigits,
                    price: order.price
                }})
            .then(async result => {
                console.log(result.data);
                if (result.data.ans) {
                    order.transactionId = req.body.data.transactionId;
                    order.cardLastDigits = req.body.cardLastDigits;
                    order.status = true;
                    await order.save();

                    return res.send({ans: true});
                }
                else {
                    return res.send({ans: false});
                }
            })
            .catch((e) => {
                console.log(e);
                return res.send({ans: false});

            });
    }


}));

module.exports = router;
