const express = require('express');
const router = express.Router({ mergeParams: true });
const catchAsync = require("../utils/catchAsync");
const ExpressError = require('../utils/ExpressError');
const { reviewSchema } = require('../schemas.js');
const Review = require('../models/review');
const Product = require('../models/product');

const validateReview = (req, res, next) => {
    console.log(req.body);
    const { error } = reviewSchema.validate(req.body);
    if (error) {
        const msg = error.details.map(el => el.message).join(',')
        throw new ExpressError(msg, 400)
    } else {
        next();
    }
}

router.get('/:reviewId', catchAsync(async (req, res) => {
    const review = await Review.findById(req.params.reviewId);
    res.send(review);
}))

router.post('/', validateReview, catchAsync(async (req, res) => {
    console.log(req.headers.id);
    const product = await Product.findById(req.headers.id);
    const review = await new Review(req.body.review);
    product.reviews.push(review);
    await review.save();
    await product.save();
    res.status(200);
    res.send("Successfully added the review");
}))

router.delete('/:reviewId', catchAsync(async (req, res) => {
    const { id, reviewId } = req.params;
    await Product.findByIdAndUpdate(id, { $pull: { reviews: reviewId }});
    await Review.findByIdAndDelete(reviewId);
    res.send(200);
}))


module.exports = router;
