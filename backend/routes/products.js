const express = require('express');
const router = express.Router();
const catchAsync = require("../utils/catchAsync");
const PCollection = require("../models/PCollection");
const Product = require("../models/product");

router.get('/:productId', catchAsync(async (req, res) => {
    const product = await Product.findById(req.params["productId"]);
    res.send(product);
}));

module.exports = router;
