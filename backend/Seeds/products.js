module.exports = [
    {
        name: "Consectetur",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Elit",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Phasellus",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Pretium",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Iaculis",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Feugiat",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Ligula",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Viverra",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Bibendum",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Scelerisque",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Vestibulum",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Condimentum",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Mauris",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    },
    {
        name: "Quisque",
        details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium justo a iaculis feugiat. Duis ligula urna, pretium viverra urna eget, bibendum scelerisque tortor. Vestibulum eu dolor facilisis felis tempus cursus. Proin et consectetur nunc. Quisque gravida condimentum odio. Mauris."
    }
]