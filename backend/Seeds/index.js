const mongoose = require('mongoose');
const products = require('./products');
const PCollection = require('../models/PCollection');
const Product = require('../models/product');
const Review = require('../models/review');
const Order = require('../models/order');

mongoose.connect('mongodb://localhost:27017/rafael-store', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
    console.log("Database connected");
});

const PCollections = ["computers", "phones", "headphones", "missiles", "tanks"];
const images = {
    "computers": "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/store-card-mac-nav-202110?wid=400&hei=260&fmt=png-alpha&.v=1632870674000",
    "phones": "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/store-card-13-iphone-nav-202109_GEO_US?wid=400&hei=260&fmt=png-alpha&.v=1630706116000",
    "headphones": "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/store-card-13-airpods-nav-202110?wid=400&hei=260&fmt=png-alpha&.v=1633718741000",
    "tanks": "https://www.rafael.co.il/wp-content/uploads/2019/03/main-11.jpg",
    "missiles": "https://he.rafael.co.il/wp-content/uploads/2020/01/raf.jpg"
}

const sample = array => array[Math.floor(Math.random() * array.length)];
const NUM_OF_PRODUCTS_PER_COLLECTION = 14;

const seedDB = async () => {
    await PCollection.deleteMany({});
    await Order.deleteMany({});
    await Product.deleteMany({});
    await Review.deleteMany({});

    let collectionCounter = 0;

    // generating collections
    for (const name of PCollections) {
        const collection = new PCollection({
            name: name,
            image: images[name]
        });

        // generating products for each collection
        for (let i = 0; i < NUM_OF_PRODUCTS_PER_COLLECTION; i++)
        {
            const price = Math.floor(Math.random() * 20) + 10;
            const product = new Product({
                ...products[i],
                price: price,
                image: `https://source.unsplash.com/random/1200x1200?sig=${++collectionCounter}`, // static-random image for each product
                reviews: []
            });
            collection.products.push(product);
            await product.save();
            await collection.save();
        }
    }
}

seedDB().then(() => {
    mongoose.connection.close();
})