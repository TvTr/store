const Joi = require("joi");

const zipcodeRegex = /^(([\d]{7})|([\d]{5}))$/i;
const phoneRegex = /^0(((\d{1}(-?)\d{8})|(\d{2}-\d{7}))|(\d{2}-\d{3}-\d{4}))$/;

const MONGO_ID_LEN = 24;

module.exports.reviewSchema = Joi.object({
    review: Joi.object({
        body: Joi.string().allow(''),
        name: Joi.string().required(),
        rating: Joi.number().required().min(1).max(5),
    }).required()
})

module.exports.firstStageSchema = Joi.object({
    firstStage: Joi.object({
        // name
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        // contact info
        email: Joi.string().email().required(),
        phoneNumber: Joi.string().pattern(phoneRegex).required(),
        // address
        street: Joi.string().required(),
        optionalStreet: Joi.string().allow('').required(),
        city: Joi.string().required(),
        zipcode: Joi.string().pattern(zipcodeRegex).required(),
        // products
        products: Joi.array().items(Joi.object({
            _id: Joi.string().length(MONGO_ID_LEN),
            amount: Joi.number().min(1)
        })).min(1)
    }).required()
})
