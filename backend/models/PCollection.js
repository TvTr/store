const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PCollectionSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    products: [
        {
            type: Schema.Types.ObjectId,
            ref: 'product',
            required: false
        }
    ]});



module.exports = mongoose.model('PCollection', PCollectionSchema);