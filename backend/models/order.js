const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    street: String,
    optionalStreet: String,
    city: String,
    zipcode: Number,
    phoneNumber: String,
    transactionId: String,
    cardLastDigits: Number,
    status: Boolean,
    products: [
        {
            type: Schema.Types.ObjectId,
            ref: 'product'
        }
    ],
    price: Number
});

module.exports = mongoose.model('Order', OrderSchema);