const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    details: {
      type: String,
      required: false
    },
    image: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    reviews: [
        {
            type: Schema.Types.ObjectId,
            ref: 'review',
            required: false
        }
    ]
});

module.exports = mongoose.model("Product", ProductSchema);