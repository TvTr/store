const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const mongoSanitize = require('express-mongo-sanitize');
const categoriesRouter = require('./routes/categories');
const usersRouter = require('./routes/users');
const productsRouter = require('./routes/products');
const reviewsRouter = require('./routes/reviews');
const ordersRouter = require('./routes/orders');
const ExpressError = require("./utils/ExpressError");

const app = express();

mongoose.connect('mongodb://localhost:27017/rafael-store');
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error: "));
db.once("open", () => {
  console.log("Database connected");
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
    mongoSanitize({
      onSanitize: ({ req, key }) => {
        console.warn(`This request[${key}] is sanitized`, req);
      },
    }),
);

const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, id');
  next();
}

app.use(allowCrossDomain);

app.use('/orders', ordersRouter);
app.use('/products', productsRouter);
app.use('/reviews', reviewsRouter);
app.use('/categories', categoriesRouter);
app.use('/users', usersRouter);

app.all('*', (req, res, next) => {
    next(new ExpressError('Page Not Found :(', 404));
})

app.use((err, req, res, next) => {
    const {statusCode = 500} = err;
    if (!err.message) err.message = "Aww, man. Something went wrong!";
    res.status(statusCode).send({ err });
})

app.listen(8888, () => {
  console.log('Serving on port 8888');
});

