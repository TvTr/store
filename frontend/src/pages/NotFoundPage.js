import React from 'react';
import NotFound from '../resources/404.svg';
import {Box} from "@mui/material";
import Typography from "@mui/material/Typography";

class LoginPage extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Box style={{ position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}}>
                    <img src={NotFound} style={{height: '65vh', }} alt='Oops! The Page You Were Looking For Not Found!'/>
                    <br/>
                    <Typography textAlign="center">Oops! The Page You Were Looking For Not Found!</Typography>
                </Box>
            </React.Fragment>
        )
    }
}

export default LoginPage;