import React from 'react';
import {Box, Divider, Grid, Paper, Stack, Typography} from "@mui/material";
import axios from "axios";
import {API} from "../globals";
import LoadingImage from "../resources/loading.svg";
import ErrorImage from "../resources/503.svg";
import {withRouter} from "react-router-dom";
import Button from "@mui/material/Button";
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import {connect} from "react-redux";
import {addToCart} from "../redux/actions";
import ReviewForm from "../components/Review/ReviewForm";
import ReviewCard from "../components/Review/ReviewCard";

class ProductPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            product: null,
            isError: false,
            isRefresh: false
        };
        this.receiveProductData = this.receiveProductData.bind(this);
        this.handleAddToCart = this.handleAddToCart.bind(this);
        this.rerenderCallback = this.rerenderCallback.bind(this);
    }

    componentDidMount() {
        const productId = this.props.match.params.productId;
        this.receiveProductData(productId);
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.isRefresh)
        {
            this.setState({
                isRefresh: false
            });
            this.componentDidMount();
        }
    }

    async receiveProductData (productId) {
        try {
            const res = await axios.get(`${API}/products/${productId}/`);
            this.setState({
                product: res.data
            })
        } catch (err) {
            console.log(err);
            this.setState({
                isError: true
            })
        }
    }

    rerenderCallback() {
        this.setState({
            isRefresh: true
        });
        this.forceUpdate();
    }

    handleAddToCart = () => {
        this.props.addToCart({
            ...this.state.product,
            amount: 1
        });
    };

    render() {
        // product not defined - showing loading screen
        if (this.state.isError)
        {
            return <React.Fragment>
                <Grid container spacing={2}>
                    <img src={ErrorImage} style={{ height: '65vh', position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}} alt={"Oops! The Service Is Unavailable"}/>
                </Grid>
            </React.Fragment>
        }
        else if (this.state.product === null)
        {
            return (
                <React.Fragment>
                    <img src={LoadingImage} style={{ height: '65vh', position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}} alt={"Loading data..."}/>
                    <h3 style={{textAlign: "center"}}>Loading Product Data...</h3>
                </React.Fragment>
            );
        }
        else
        {
            return (
                <Paper spacing={{ xs: 1, sm: 2, md: 4 }} style={{minHeight: "100vh", margin: "1em"}}>
                    <Stack direction={{ xs: 'column', sm: 'row' }}
                           spacing={0}>
                        <Box sx={{width: {xs: '100%'}, height: {xs: '100%'}}}>
                            <img src={this.state.product.image} style={{width: '100%'}} alt={this.state.product.name}/>
                        </Box>
                        <Stack sx={{width: {xs: '82%', sm: '60vw'}}} direction="column" style={{textAlignHorizontal: "center", alignContent: "top", justifyContentHorizontal: "center", marginTop: "1em", paddingRight: "5em"}}>
                            <Typography variant="h3" style={{fontSize: "2em", alignContent: "center", justifyContent: "center", marginLeft: "0.46em", maxWidth: '100%'}}>
                                {this.state.product.name}
                            </Typography>
                            <Box style={{ margin: "1em", }}>
                                <Typography paragraph style={{fontSize: "1em", alignContent: "center", justifyContent: "center", }}>
                                    {this.state.product.details}
                                </Typography>
                                <Typography variant="h4" style={{fontSize: "1.5em", alignContent: "center", justifyContent: "center", marginTop: "0.5em"}}>
                                    {this.state.product.price + "$"}
                                </Typography>
                                <Button variant="contained" onClick={this.handleAddToCart} startIcon={<AddShoppingCartIcon />} style={{marginTop: "1.5em"}}>
                                    Buy Now
                                </Button>
                            </Box>
                            <Divider style={{margin: "1em"}}/>
                            <ReviewForm id={this.props.match.params.productId} rerenderParentCallback={this.rerenderCallback}/>
                            <Divider  variant="middle" style={{margin: "1.5em"}}/>
                            <Typography variant="h3" style={{fontSize: "1.5em", fontWeight: 300, marginLeft: "1em"}}>
                                Reviews
                            </Typography>
                            <Grid>
                                {
                                    this.state.product.reviews.map((reviewId) => (
                                        <Grid item key={reviewId} xs={12} sm={6} md={4} align="center">
                                            <ReviewCard key={reviewId + "/"} reviewId={reviewId}/>
                                        </Grid>))
                                }
                            </Grid>
                        </Stack>
                    </Stack>
                </Paper>
            )
        }

    }
}

export default withRouter(connect(
    null,
    { addToCart }
)(ProductPage));
