import React from 'react';
import { withRouter } from "react-router-dom";
import axios from "axios";
import {API} from '../globals';
import LoadingImage from '../resources/loading.svg';
import {Grid} from "@mui/material";
import ErrorImage from "../resources/503.svg";
import ProductCard from "../components/ProductCard";

class CategoryPage extends React.Component {
    constructor(props) {
        super(props);
        this.generateProducts=this.generateProducts.bind(this);
        this.state = {
            products: [],
            isError: false
        };
    }

    componentDidMount() {
        this._isMounted = true;
        const categoryName = this.props.match.params.categoryName;
        this.generateProducts(categoryName)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.match.params.categoryName !== this.props.match.params.categoryName) {
            this.componentDidMount();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    async generateProducts (categoryName) {
        try {
            const res = await axios.get(`${API}/categories/${categoryName.toLowerCase()}/`);
            if (this._isMounted)
            {
                this.setState({
                    products: res.data
                })
            }

        } catch (err) {
            console.log(err);
            this.setState({
                isError: true
            })
        }
    }

    render() {
        if (this.state.isError)
        {
            return <div style={{marginTop: "3vh"}}>
                <Grid container spacing={2}>
                    <img src={ErrorImage} style={{ height: '65vh', position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}} alt={"Oops! The Service Is Unavailable"}/>
                </Grid>
            </div>
        }
        else if (!this.state.products.length)
        {
            return (
                <div style={{marginTop: "3vh"}}>
                        <img src={LoadingImage} style={{ height: '65vh', position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}} alt={"Loading data..."}/>
                        <h3 style={{textAlign: "center"}}>Loading Products...</h3>
                </div>
            );
        }
        else
        {
            return (
                    <Grid container
                          spacing={4}
                          direction="row"
                          style={{ paddingBottom: "3vh", padding: "0.5em 0.5em 5vh 0.5em", marginTop: "1vh"}}
                          justifyContent="center"
                          >
                        {
                            this.state.products.map((product) => (
                                <Grid item key={product.name} xs={12} sm={6} md={4} align="center">
                                    <ProductCard key={product.name + "/"} category={this.props.match.params.categoryName} product={product}/>
                                </Grid>))
                        }
                    </Grid>

            )
        }
    }
}

export default withRouter(CategoryPage);
