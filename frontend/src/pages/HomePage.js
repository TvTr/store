import React from 'react';
import {Grid} from "@mui/material";
import ErrorImage from '../resources/503.svg';
import axios from "axios";
import CollectionCard from "../components/CollectionCard";
import {API} from '../globals';

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: []
        };
        this.generateCategories=this.generateCategories.bind(this);

    }

    componentDidMount() {
        this._isMounted = true;
        this.generateCategories();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }


    generateCategories = async() => {
        try {
            const res = await axios.get(`${API}/categories/`);
            const categories = res.data["PCollectionsFiltered"];
            // this will re render the view with new data
            this.setState({
                categories: categories
            });
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        if (this.state.categories && this.state.categories.length > 0)
        {
            return (
                    <Grid container
                          spacing={7}
                          direction={{xs: "column", md: "row"}}
                          alignItems="center"
                          justifyContent="center"
                          sx={{padding: "3vh"}}>
                        {
                            this.state.categories.map((category, index) => (
                                <Grid item key={category.name} xs={4}>
                                    <CollectionCard image={category.image} name={category.name} index={index}/>
                                </Grid>))
                        }

                    </Grid>
            )

        }
        else {
            return <div style={{marginTop: "3vh"}}>
                <Grid container spacing={2}>
                    <img src={ErrorImage} style={{ height: '65vh', position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}} alt={"Oops! The Service Is Unavailable"}/>
                </Grid>
            </div>
        }


    }
}

export default HomePage;