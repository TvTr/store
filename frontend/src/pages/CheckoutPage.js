import React from "react";
import {connect} from "react-redux";
import {mapStateToProps} from "../redux/selectors";
import EmptyCart from "../components/EmptyCart";
import CheckoutFirstStage from "../components/Checkout/CheckoutFirstStage";
import CheckoutSecondStage from "../components/Checkout/CheckoutSecondStage";
import CompleteImage from "../resources/order_completed.jpg";
import {Stack} from "@mui/material";
import {Typography} from "@material-ui/core";

const FIRST_STAGE = 1;
const SECOND_STAGE = 2;
const COMPLETE = 3;

class CheckoutPage extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            checkoutStage: FIRST_STAGE,
            checkoutPrice: 0,
            checkoutId: null,
            status: false
        }
        this.moveStage = this.moveStage.bind(this);
        this.complete = this.complete.bind(this);
    }

    complete()
    {
        this.setState({
            checkoutStage: COMPLETE
        });
    }

    moveStage(checkoutId, price) {
        console.log(checkoutId);
        this.setState({
            checkoutStage: SECOND_STAGE,
            checkoutId: checkoutId,
            checkoutPrice: price
        })
    }

    render() {
        if (Object.keys(this.props.products).length < 1)
        {
            return <EmptyCart/>;
        }
        else if (this.state.checkoutStage === FIRST_STAGE)
        {
            return <CheckoutFirstStage moveStage={this.moveStage}/>;
        }
        else if (this.state.checkoutStage === SECOND_STAGE)
        {
            return <CheckoutSecondStage price={this.state.checkoutPrice} id={this.state.checkoutId} complete={this.complete}/>
        }
        else if (this.state.checkoutStage === COMPLETE)
        {
            return <Stack sx={{marginTop: "3vh"}}>
                <Typography variant="h1" style={{fontSize: "1.3em", fontWeight: 600, textAlign: "center"}}>Thank you for ordering with Rafael Online Store</Typography>
                <img src={CompleteImage} style={{maxHeight: "70vh", objectFit: "contain"}} alt={"Order Complete!"}/>
                <Typography variant="h1" style={{fontSize: "1.3em", fontWeight: 400, textAlign: "center"}}>Your Order ID: {this.state.checkoutId}</Typography>
            </Stack>
        }
    }


}

export default connect(
    mapStateToProps,
    null
)(CheckoutPage);