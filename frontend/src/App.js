import {Route, Switch} from "react-router-dom";
import HomePage from "./pages/HomePage";
import Layout from './components/layout/Layout';
import NotFoundPage from "./pages/NotFoundPage";
import CategoryPage from "./pages/CategoryPage";
import ProductPage from "./pages/ProductPage";
import CheckoutPage from "./pages/CheckoutPage";


function App() {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact>
                    <HomePage/>
                </Route>
                <Route path="/categories/:categoryName" exact>
                    <CategoryPage/>
                </Route>
                <Route path="/categories/:categoryName/:productId" exact>
                    <ProductPage/>
                </Route>
                <Route path="/checkout" exact>
                    <CheckoutPage/>
                </Route>
                <Route path="*" >
                    <NotFoundPage/>
                </Route>
            </Switch>
        </Layout>
    );
}

export default App;
