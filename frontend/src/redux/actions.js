import { ADD_TO_CART, REMOVE_FROM_CART, EMPTY_CART, DECREASE_FROM_CART } from "./actionTypes";

export const addToCart = content => ({
    type: ADD_TO_CART,
    payload: {
        content
    }
});

export const removeFromCart = content => ({
    type: REMOVE_FROM_CART,
    payload: {
        content
    }
});

export const emptyCart = () => ({
    type: EMPTY_CART,
    payload: {}
});

export const decreaseFromCart = content => ({
    type: DECREASE_FROM_CART,
    payload: {
        content
    }
})