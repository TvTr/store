import {ADD_TO_CART, REMOVE_FROM_CART, EMPTY_CART, DECREASE_FROM_CART} from "../actionTypes";

const initialState = {
    products: {}
};

export default function(state = initialState, action) {
    switch (action.type) {
        case ADD_TO_CART: {
            let { content } = action.payload;

            if (content._id in state.products) {
                content.amount += state.products[content._id].amount
            }
            return {
                ...state,
                products: {
                    ...state.products,
                    [content._id]: content
                    }
            };
        }
        case DECREASE_FROM_CART: {
            const id = action.payload.content._id;
            if (id in state.products)
            {
                return {
                    ...state,
                    products: {
                        ...state.products,
                        [id]: {
                            ...state.products[id],
                            amount: state.products[id].amount - 1
                        }
                    }
                };
            }
            else
            {
                return state;
            }
        }
        case REMOVE_FROM_CART: {
            const id = action.payload.content._id;
            const stateProducts = Object.entries(state.products);
            const filtered = stateProducts.filter(([key]) => key !== id);
            return {
                ...state,
                products: Object.fromEntries(filtered)
            };
        }
        case EMPTY_CART: {
            return {
                ...state,
                products: {}
            }
        }
        default:
            return state;
    }
}
