import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { grey } from '@mui/material/colors';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import {addToCart} from "../redux/actions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

const MAX_LEN = 80;


const theme = createTheme({
    palette: {
        primary: {
            main: "#285f9c",
        },
        secondary: {
            main: grey[800]
        },
    },
});

class ProductCard extends React.Component {
    constructor(props) {
        super(props);
        this.handleAddToCart = this.handleAddToCart.bind(this);
    }

    handleAddToCart = () => {
        this.props.addToCart({
            ...this.props.product,
            amount: 1
        });
    };

    render()
    {
        return(
            <ThemeProvider theme={theme}>
                <Card sx={{ maxWidth: 345 }}>
                    <CardMedia
                        component="img"
                        alt={this.props.product.name}
                        height="140"
                        image={this.props.product.image}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {this.props.product.name} - {this.props.product.price}$
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {this.props.product.details.substring(0, MAX_LEN) + "..."}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="small" variant="contained" color="primary" startIcon={<AddShoppingCartIcon /> } onClick={this.handleAddToCart}>Buy Now</Button>
                        <Link to={`/categories/${this.props.category}/${this.props.product._id}`} style={{textDecoration: 'none', marginLeft: '0.75em'}}>
                            <Button size="small" color="secondary">Learn More</Button>
                        </Link>
                    </CardActions>
                </Card>
            </ThemeProvider>
        )
    }
}

export default connect(
    null,
    { addToCart }
)(ProductCard);