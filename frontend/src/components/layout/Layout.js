import {Fragment} from "react";
import MainNavigator from "./MainNavigator";

const Layout = (props) => {
    return (
        <Fragment >
            <header>
                <MainNavigator style={{marginTop: "10vh"}}/>
            </header>
            <main style={{ justifyContent:'center', alignItems:'center'}}>
                    {props.children}
            </main>
        </Fragment>
    );
};

export default Layout;