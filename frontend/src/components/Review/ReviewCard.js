import React from "react";
import {Card, Rating, Stack, Typography} from "@mui/material";
import axios from "axios";
import {API} from "../../globals";
import ErrorImage from '../../resources/error.svg';

export default class ReviewCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            review: null,
            isError: false
        };
        this.receiveReviewData = this.receiveReviewData.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        if (this.props.reviewId)
        {
            this.receiveReviewData(this.props.reviewId);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    async receiveReviewData(reviewId) {
        try
        {
            const res = await axios.get(`${API}/reviews/${reviewId}/`);
            if (this._isMounted)
            {
                this.setState({
                    review: res.data
                });
            }

        } catch (e)
        {
            console.log(e);
            this.setState({
                isError: true
            })
        }
    }

    render() {
        if (this.state.isError) {
            return <img src={ErrorImage} alt="An Error Occurred"/>
        }
        else if (this.state.review) {
            return <Card style={{backgroundColor: "#eeecec", margin: "1.5em", minWidth: "30vw", minHeight: "10vh"}}>
                <Stack>
                    <Typography variant="h4" style={{
                        fontSize: "1em",
                        fontWeight: (this.state.ratingError ? 400 : 300),
                        marginLeft: "1em",
                        marginTop: "1em",
                        marginBottom: "0.5em",
                        textAlign: "left",
                        color: (this.state.ratingError ? '#c23f38' : 'black')
                    }}>
                        {this.state.review.name}
                    </Typography>
                    <Rating name="read-only" value={this.state.review.rating} readOnly
                            style={{marginLeft: "0.65em", marginBottom: "0.5em"}}/>
                    <Typography variant="body"
                                style={{fontSize: "0.75em", marginLeft: "1.65em", paddingBottom: "1em", fontWeight: 400, textAlign: "left"}}>
                        {this.state.review.body ? this.state.review.body : "This review has only rating."}
                    </Typography>
                </Stack>
            </Card>
        }
        else
        {
            return <></>;
        }
    }
}