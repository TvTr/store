import React from 'react';
import {Button, Rating, Stack, TextField, Typography} from "@mui/material";
import {API} from "../../globals";
import ErrorImage from "../../resources/503.svg";
import tickMark from '../../resources/tickMark.gif';
import axios from "axios";
const ERR_TIMEOUT = 5000;
const SUCCESS_TIMOUT = 1300;

export default class ReviewForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            nameError: false,
            rating: 0,
            ratingError: false,
            body: "",
            isError: false,
            isSuccess: false
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.handleError = this.handleError.bind(this);
        this.handleSuccess = this.handleSuccess.bind(this);
    }

    async onSubmit(e) {
        e.preventDefault();

        // validating name and rating (body is optional)
        if (this.state.name.length < 1) {
            console.log(this.state.name)
            this.setState({
                nameError: true
            });
            if (!this.state.rating) {
                this.setState({
                    ratingError: true
                });
            }
        } else if (!this.state.rating) {

            this.setState({
                ratingError: true
            });
        } else {
            this.setState({
                ratingError: false,
                nameError: false
            })
            // submit
            const review = {
                review: {
                    name: this.state.name,
                    rating: this.state.rating,
                    body: this.state.body
                }
            }
            console.log(review);
            axios.post(`${API}/reviews`, review,
                {
                    headers: {
                        id: this.props.id
                    }
                }
            )
                .then(this.handleSuccess)
                .catch(this.handleError);
        }

    }

    handleSuccess() {
        this.setState({
            isSuccess: true
        });
        setTimeout(() => {
            this.setState({
                name: "",
                rating: 0,
                body: "",
                isError: false,
                isSuccess: false
            });
            this.props.rerenderParentCallback();
        }, SUCCESS_TIMOUT);
    }

    handleError() {
        this.setState({
            isError: true
        });
        setTimeout(() => {
            this.setState({
                name: "",
                rating: 0,
                body: "",
                isError: false,
                isSuccess: false
            })
        }, ERR_TIMEOUT);

    }

    render() {
        if (this.state.isError)
        {
            return (<img src={ErrorImage} style={{ height: '24.5em', marginLeft: '1em'}} alt={"Oops! The Service Is Unavailable"}/>
            );
        }
        else if (this.state.isSuccess)
        {
            return (<img src={tickMark} style={{ maxHeight: '24.5em', marginLeft: '1em'}} alt={"Oops! The Service Is Unavailable"}/>
            );
        }
        return (
            <>
                <Stack direction="column">
                    <Typography variant="h3" style={{fontSize: "1.5em", fontWeight: 300, marginLeft: "1em"}}>
                        Leave A Review
                    </Typography>
                    <TextField style={{marginTop: "1em", marginLeft: "1.5em"}}
                               error={this.state.nameError}
                               required
                               value={this.state.name}
                               onChange={(e) => {
                                   this.setState({
                                       name: e.target.value
                                   })
                               }}
                               id="outlined-required"
                               helperText={this.state.nameError ? "Enter your name." : ""}
                               label="Name"
                    />
                    <Stack direction="row">
                        <Typography variant="h4" style={{fontSize: "1em", marginLeft: "1.65em", fontWeight: (this.state.ratingError ? 400 : 300),  marginTop: "1em", color: (this.state.ratingError ? '#c23f38' : 'black')}}>
                            Your Rating:
                        </Typography>
                        <Rating
                            name="simple-controlled"
                            value={this.state.rating}
                            style={{marginTop: "0.52em", marginLeft: "0.5em"}}
                            onChange={(event, newValue) => {
                                this.setState({
                                    rating: newValue
                                });
                            }}
                        />
                    </Stack>
                    <TextField
                        style={{marginTop: "1em", marginLeft: "1.5em"}}
                        id="outlined-multiline-static"
                        value={this.state.body}
                        onChange={(e) => {
                            this.setState({
                                body: e.target.value
                            })
                        }}
                        label="Body"
                        multiline
                        rows={4}
                    />
                    <Button variant="contained" style={{marginLeft: "1.8em", marginTop: "2em"}} onClick={this.onSubmit}>Submit Review</Button>
                </Stack>

            </>
        )
    }
}