import React from "react";
import {Button, Divider, Stack} from "@mui/material";
import {Typography} from "@material-ui/core";
import LogoImage from '../resources/logo.svg';
import ShoppingBasketIcon from '@mui/icons-material/ShoppingBasket';
import {Link} from "react-router-dom";

export default class EmptyCart extends React.Component {
    render() {
        return <Stack direction="column"
                      style={{ paddingBottom: "3vh", padding: "0.5em 0.5em 5vh 0.5em", marginTop: "5vh"}}
                      justifyContent="center">
            <Typography variant="h1" style={{fontSize: "2.5em", marginLeft: "7vw", fontWeight: 200}}>Oops. Your cart is empty. </Typography>
            <Typography variant="body1" style={{fontSize: "1.5em", marginLeft: "7.01vw", fontWeight: 300}}>Go and shop something to continue the checkout process.</Typography>
            <Button
                component={Link} to={"/"} size="big" color="primary" variant="contained" sx={{width: {xs: "80vw", sm: "40vw", md: "30vw", lg: "25vw"}, maxHeighteight: "7vh", height: {sm: "4vh", md: "4.5vh"}, marginLeft: "7vw", marginTop: "3vh", textTransform: "none", borderRadius: "0.7em"}} startIcon={<ShoppingBasketIcon /> } autoFocus>
                Continue Shopping
            </Button>
            <Divider style={{marginTop: "7vh"}}/>
            <Typography style={{fontSize: "1.3em", marginLeft: "6vw", fontWeight: 400, marginTop: "1vh"}}>Need some help? Call 1-800-MY-RAFAEL customer service 24/7</Typography>
            <Divider style={{marginTop: "1vh"}}/>
            <img src={LogoImage} alt={"Rafael Logo"} style={{marginTop: "5vh", maxHeight: "50vh", alignSelf: "center"}}/>
        </Stack>
    }

}