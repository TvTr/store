import React from 'react';
import { Stack, TextField} from "@mui/material";

export default class AddressForm extends React.Component {
    render() {
        return (
            <>
                <Stack direction="column" sx={{width: {md: "45vw", lg: "35vw", sm: "65vw", xs: "90vw"}, marginLeft: {md: "5.4vw", sm: "5vw", xs: "2vw"}}}>
                    <TextField sx={{marginTop: "1em", marginLeft: {md: "2.1vw"}}}
                               required
                               error={this.props.state.firstNameError}
                               value={this.props.state.firstName}
                               onChange={(e) => {this.props.onChange(e, "firstName")}}
                               id="outlined-required"
                               helperText={this.props.state.firstNameError ? "Enter your first name." : ""}
                               label="First Name"
                    />
                    <TextField sx={{marginTop: "1em", marginLeft: {md: "2.1vw"}}}
                               error={this.props.state.lastNameError}
                               required
                               value={this.props.state.lastName}
                               onChange={(e) => {this.props.onChange(e, "lastName")}}
                               id="outlined-required"
                               helperText={this.props.state.lastNameError ? "Enter your last name." : ""}
                               label="Last Name"
                    />
                    <TextField sx={{marginTop: "1em", marginLeft: {md: "2.1vw"}}}
                               error={this.props.state.streetError}
                               required
                               value={this.props.state.street}
                               onChange={(e) => {this.props.onChange(e, "street")}}
                               id="outlined-required"
                               helperText={this.props.state.streetError ? "Enter your street." : ""}
                               label="Street Address"
                    />
                    <TextField sx={{marginTop: "1em", marginLeft: {md: "2.1vw"}}}
                               onChange={(e) => {this.props.onChange(e, "optional")}}
                               id="outlined-required"
                               label="Apt, Suite, Building (Optional)"
                    />
                    <Stack direction="row">
                        <TextField sx={{marginTop: "1em", marginLeft: {md: "2.1vw"}, width: "50%"}}
                                   error={this.props.state.zipcodeError}
                                   required
                                   value={this.props.state.zipcode}
                                   onChange={(e) => {this.props.onChange(e, "zipcode")}}
                                   id="outlined-required"
                                   helperText={this.props.state.zipcodeError ? "Enter a valid zipcode (5 or 7 digits)." : ""}
                                   label="Zip Code"
                        />
                        <TextField sx={{marginTop: "1em", marginLeft: "1.5vw", width: "50%"}}
                                   error={this.props.state.cityError}
                                   required
                                   value={this.props.state.city}
                                   onChange={(e) => {this.props.onChange(e, "city")}}
                                   id="outlined-required"
                                   helperText={this.props.state.cityError ? "Enter your city." : ""}
                                   label="City"
                        />
                    </Stack>
                </Stack>

            </>
        )
    }
}