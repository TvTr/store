import * as React from 'react';
import Stack from '@mui/material/Stack';
import {IconButton, Typography} from "@mui/material";
import Paper from '@mui/material/Paper';
import IndeterminateCheckBoxIcon from '@mui/icons-material/IndeterminateCheckBox';
import Button from "@mui/material/Button";
import {connect} from "react-redux";
import {decreaseFromCart} from "../../redux/actions";
import {mapStateToProps} from "../../redux/selectors";
import {Box} from "@material-ui/core";
import RemoveShoppingCartIcon from '@mui/icons-material/RemoveShoppingCart';

class CartItem extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.handleDecrease = this.handleDecrease.bind(this);
        this.onRemoveProduct = this.onRemoveProduct.bind(this);

    }

    handleDecrease = (e) => {
        e.preventDefault();
        const product = this.props.products[this.props.productId];

        if (product.amount > 1)
        {
            this.props.decreaseFromCart(product);
        }
    }

    onRemoveProduct = (e) => this.props.handleRemove(e, this.props.products[this.props.productId]);

    render()
    {
        const product = this.props.products[this.props.productId];
        const isDisabled = (product.amount > 1 ? false : true);

        return (
            <Paper>
                <Stack direction={{ xs: 'column', sm: 'row' }}
                       spacing={"3.5em"} style={{justifyContent: "center"}}>
                    <Box
                        component="img"
                        sx={{
                            maxHeight: {sm: "15em" },
                            maxWidth: {xs: "70vw"}
                        }}
                        src={product.image}
                    />

                    <Stack direction="column" sx={{textAlignHorizontal: "center", alignContent: "top", justifyContentHorizontal: "center", paddingTop: {sm: "3vh"}, paddingRight: {sm: "7.5vw", xs: "1.5vw"}}}>
                        <Typography variant="h6">
                            {product.name}
                        </Typography>
                        <Stack direction="row" sx={{marginTop: {xs: "1vh", sm: "2vh"}, justifyContent: {xs: "center"}}}>
                            <Typography sx={{textAlign: "center", alignContent: "center", paddingTop: "0.85vh"}}>
                                Amount: {product.amount}
                            </Typography>
                            <IconButton disabled={isDisabled} onClick={this.handleDecrease}>
                                <IndeterminateCheckBoxIcon/>
                            </IconButton>
                        </Stack>
                        <Button size="small" variant="outlined" color="primary" startIcon={<RemoveShoppingCartIcon /> } onClick={this.onRemoveProduct} sx={{marginTop: "2em", marginLeft: {xs: "8vw", sm: "0vw"}, marginRight: {xs: "8vw", sm: "0vw"}, marginBottom: {xs: "3vh", sm: "0vw"}}}>
                            Remove
                        </Button>
                    </Stack>
                </Stack>
            </Paper>
        )
    }
}

export default connect(
    mapStateToProps,
    { decreaseFromCart }
)(CartItem);