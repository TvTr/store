import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import {styled} from '@mui/material/styles';
import {Divider, Grid, Stack, Typography} from "@mui/material";
import {connect} from "react-redux";
import CartItem from "./CartItem";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import IconButton from "@mui/material/IconButton";
import Badge from "@mui/material/Badge";
import {mapStateToProps} from "../../redux/selectors";
import {decreaseFromCart, emptyCart, removeFromCart} from "../../redux/actions";
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';
import RemoveShoppingCartIcon from '@mui/icons-material/RemoveShoppingCart';
import CloseFullscreenIcon from '@mui/icons-material/CloseFullscreen';
import {Link} from "react-router-dom";

const StyledBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
        right: -3,
        top: 13,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
    },
}));

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            products: this.props.products
        };
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.renderCartItems = this.renderCartItems.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleRerender = this.handleRerender.bind(this);
    }

    handleOpen = () => {
        this.setState({
            open: true
        });
    };

    handleClose = (e) => {
        this.setState({
            open: false
        });
    };

    handleRemove = (e, product) => {
        e.preventDefault();
        this.props.removeFromCart(product);
    }

    handleRerender = () => {
        this.forceUpdate();
    }

    renderCartItems = (key, index) => {
        return (
            <Grid item key={key} sx={{alignContent: "center", justifyContent: "center", marginLeft: {sm: "1.8vw"}}}
                  align="center">
                <CartItem handleRemove={this.handleRemove} key={key + " "} productId={key}/>
            </Grid>);
    }

    render() {
        return (
            <div>
                <IconButton aria-label="cart" onClick={this.handleOpen}>
                    <StyledBadge badgeContent={Object.keys(this.props.products).length} color="secondary">
                        <ShoppingCartIcon />
                    </StyledBadge>
                </IconButton>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="cart"

                >
                    <DialogTitle id="cart-title">
                        {"Cart"}
                    </DialogTitle>
                    <DialogContent>
                        <Grid container spacing={"2em"}>
                            {
                                Object.keys(this.props.products).length > 0 ?
                                    Object.keys(this.props.products).map(this.renderCartItems)
                                    : <Typography style={{marginTop: "2.5em", marginLeft: "2.5em", fontSize: "1em"}}>The Cart Is Empty</Typography>
                            }
                        </Grid>
                        <Divider variant="middle" sx={{marginTop: "2vh"}}/>
                    </DialogContent>
                    <DialogActions style={{justifyContent: "center"}}>
                        <Stack direction={{ xs: 'column', sm: 'row' }} spacing={{xs: "0.5em"}}>
                            <Button size="small" onClick={this.handleClose} startIcon={<CloseFullscreenIcon /> } autoFocus>
                                Close
                            </Button>
                            <Button size="small" onClick={this.props.emptyCart}
                                    disabled={!Object.keys(this.props.products).length} variant="contained"
                                    color="secondary" startIcon={<RemoveShoppingCartIcon /> } autoFocus>
                                Empty Cart
                            </Button>
                            <Button component={Link} to="/checkout" onClick={this.handleClose}
                                    size="small" variant="contained" disabled={!Object.keys(this.props.products).length} color="primary"
                                    startIcon={<ShoppingCartCheckoutIcon /> }>
                                Checkout
                            </Button>
                        </Stack>


                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    { removeFromCart, decreaseFromCart, emptyCart }
)(Cart);