import React from 'react';
import {Box, Button, Stack, TextField} from "@mui/material";
import Cards from "react-credit-cards";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DateAdapter from "@mui/lab/AdapterMoment";
import DatePicker from "@mui/lab/DatePicker";
import moment from "moment";
import * as PropTypes from "prop-types";
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

DatePicker.propTypes = {
    label: PropTypes.string,
    minDate: PropTypes.any,
    onChange: PropTypes.func,
    views: PropTypes.arrayOf(PropTypes.string),
    maxDate: PropTypes.any,
    renderInput: PropTypes.func
};

export default class PaymentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cvc: '',
            expiryDate: null,
            expiry: '',
            focus: '',
            name: '',
            number: '',
            /* errors*/
            cvcError: false,
            nameError: false,
            numberError: false,
            expiryError: false
        };
        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.verifyData = this.verifyData.bind(this);
    }

    handleInputFocus = (e) => {
        this.setState({ focus: e.target.id });
    }

    handleInputChange = (e) => {
        let {id, value} = e.target;
        // cutting the value to the required length
        switch (id) {
            case "number":
                value =  value.slice(0,19);
                break;
            case "cvc":
                value =  value.slice(0,4);
                break;
            default:
                break;
        }
        this.setState({ [id]: value });
    }

    verifyData() {
        let verified = true;
        const changeState = (property, value) => {
            verified = verified ? !value : verified;
            this.setState({
                [`${property}Error`]: value
            })
        }
        changeState('number', !validateCardNumber(this.state.number));
        changeState('cvc', this.state.cvc.length != 3 && this.state.cvc.length != 4);
        changeState('name', !(this.state.name.length && this.state.name.includes(" ")));
        changeState('expiry', this.state.expiry.length != 5);
        return verified;
    }

    handleSubmit() {
        if (this.verifyData())
        {
            this.props.pay({
                number: this.state.number,
                cvc: this.state.cvc,
                name: this.state.name,
                expiry: this.state.expiry
            })
        }
    }

    render() {
        return (
            <>
                <Stack direction={{md: "row", xs: "column"}} sx={{marginLeft: {md: "7vw"}, marginTop: "3vh", width: {xs: "90vw", md: "auto"}}}>
                    <Box sx={{display: "flex", justifyContent: "left", alignItems: "left"}}>
                        <Cards
                            cvc={this.state.cvc}
                            expiry={this.state.expiry}
                            focused={this.state.focus}
                            name={this.state.name}
                            number={this.state.number}
                        />
                    </Box>

                    <Stack direction="column" sx={{marginLeft: {md: "2vw"}, marginTop: {xs: "5vh", md: "0vw"}, display: "flex", alignItems: "left"}}>
                        <TextField
                                   required
                                   error={this.state.numberError}
                                   value={this.state.number}
                                   id="number"
                                   onFocus={this.handleInputFocus}
                                   type="number"
                                   label="Credit Card"
                                   onChange={this.handleInputChange}
                                   helperText={this.state.numberError ? "Enter a valid credit card number." : ""}
                        />
                        <TextField sx={{marginTop: "1vh"}}
                                   required
                                   id="name"
                                   error={this.state.nameError}
                                   onFocus={this.handleInputFocus}
                                   value={this.state.name}
                                   onChange={this.handleInputChange}
                                   label="Full Name"
                                   helperText={this.state.nameError ? "Enter your full name." : ""}
                        />
                        <Stack direction="row">
                            <LocalizationProvider dateAdapter={DateAdapter}>
                                <DatePicker
                                    views={['month', 'year']}
                                    label="Valid Thru"
                                    minDate={new moment()}
                                    inputFormat='MM/YY'
                                    value={this.state.expiryDate}
                                    onChange={(newValue) => {
                                        this.setState({
                                            expiryDate: newValue,
                                            expiry: moment(newValue).format('MM/YY')
                                        });
                                    }}
                                    renderInput={(params) => <TextField {...params} value={this.state.expiry}
                                                                        sx={{marginTop: "1vh", width: "49%"}}
                                                                        onFocus={this.handleInputFocus}
                                                                        helperText={this.state.expiryError ? "Enter your credit card expiry date." : ""}
                                    />}
                                />
                            </LocalizationProvider>
                            <TextField sx={{marginTop: "1vh", marginLeft: "2%", width: "49%"}}
                                       required
                                       id="cvc"
                                       label="CVV/CVC"
                                       onFocus={this.handleInputFocus}
                                       value={this.state.cvc}
                                       error={this.state.cvcError}
                                       onChange={this.handleInputChange}
                                       helperText={this.state.cvcError ? "Enter your credit card cvc / cvv." : ""}
                                       type="number"
                            />
                        </Stack>
                    </Stack>
                </Stack>
                <Button onClick={this.handleSubmit} startIcon={<AttachMoneyIcon/>} sx={{display: "flex", justifyContent: "center", width: {md: "285px", xs: "85%"}, marginLeft: {md: "7.25vw", xs: "5.5vw"}, marginTop: "3vh", backgroundColor: "#01075e"}} variant="contained">Pay Now</Button>

            </>
        )
    }
}

const validateCardNumber = number => {
    //Check if the number contains only numeric value
    //and is of between 13 to 19 digits
    const regex = new RegExp("^[0-9]{13,19}$");
    if (!regex.test(number)){
        return false;
    }

    return luhnCheck(number);
}

const luhnCheck = val => {
    let checksum = 0; // running checksum total
    let j = 1; // takes value of 1 or 2

    // Process each digit one by one starting from the last
    for (let i = val.length - 1; i >= 0; i--) {
        let calc = 0;
        // Extract the next digit and multiply by 1 or 2 on alternative digits.
        calc = Number(val.charAt(i)) * j;

        // If the result is in two digits add 1 to the checksum total
        if (calc > 9) {
            checksum = checksum + 1;
            calc = calc - 10;
        }

        // Add the units element to the checksum total
        checksum = checksum + calc;

        // Switch the value of j
        if (j === 1) {
            j = 2;
        } else {
            j = 1;
        }
    }

    //Check if it is divisible by 10 or not.
    return (checksum % 10) === 0;
}