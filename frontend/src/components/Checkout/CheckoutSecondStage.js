import React from "react";
import {Alert, Box, Divider, Grid, Stack} from "@mui/material";
import {Typography} from "@material-ui/core";
import LogoImage from "../../resources/logo.svg";
import HttpsIcon from '@mui/icons-material/Https';
import 'react-credit-cards/es/styles-compiled.css';
import PaymentForm from "./PaymentForm";
import ProcessingPaymentGif from '../../resources/processing_payment.gif';
import axios from "axios";
import {API, EXTERNAL_CLEARING_SERVICE} from "../../globals";

const LAST_DIGITS = 4;

export default class CheckoutSecondStage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: false,
            isError: false,
            billingInfo: {}
        }
        this.pay = this.pay.bind(this);
        this.handleSuccess = this.handleSuccess.bind(this);
        this.completeCheckout = this.completeCheckout.bind(this);
    }

    handleSuccess(res) {
        // making sure the server returned
        if (res.data._id && !res.data.err) {
            this.setState({
                status: true
            });

            // sending the transaction id to the backend in order to complete the checkout
            setTimeout(
                () => {this.completeCheckout(res.data._id)}
                , 2500);
        }
        else {
            this.setState({
                status: false,
                isError: true
            })
        }
    }

    completeCheckout(id) {
        axios.post(`${API}/orders/secondStage`, {data: {
                ...this.state.billingInfo,
                _id: this.props.id,
                transactionId: id
            }
        })
            .then(result => {
                if (result.data.ans) {
                    this.props.complete();
                }
                else {
                    this.setState({
                        isError: true,
                        status: false
                    })
                }
            })
            .catch(() => {
                this.setState({
                    isError: true,
                    status: false
                })
            });
    }

    pay(creditData) {
        this.setState({
            status: true,
            billingInfo: {
                lastDigits: creditData.number.slice(creditData.number.length - LAST_DIGITS),
                lastName: creditData.name.split(" ")[1]
            }
        });

        creditData = {
            billingInfo: {
            ...creditData, price: this.props.price, expiry: "01/" + creditData.expiry
            }
        };

        axios.post(`${EXTERNAL_CLEARING_SERVICE}/`, creditData)
            .then(res => {this.handleSuccess(res)})
            .catch(() => {
                this.setState({
                    isError: true,
                    status: false
                })
            });
    }

    render() {
        if (this.state.status)
        {
            return <div style={{marginTop: "3vh"}}>
                <Grid container spacing={2}>
                    <img src={ProcessingPaymentGif} style={{ height: '65vh', position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}} alt={"Processing Your Payment"}/>
                </Grid>
            </div>
        }
        return (
            <>
                {this.state.isError ? <Alert severity="error" sx={{width: "80vw", transform: 'translateX(10%)', marginTop: "5vh"}}><b>Payment failed. Try again, or use a different payment method.</b> If this error continues, don't hesitate to contact our customer support 24/7.</Alert>
                    : <div></div>}
                <Stack direction="column"
                       sx={{ paddingBottom: "3vh", padding: "0.5em 0.5em 5vh 0.5em", marginTop: {sm: "3vh", md: "5vh", xs: "1.5vh"}}}
                       justifyContent="center">

                    <Box sx={{marginLeft: {xs: "2vw", sm: "5vw", md: "7.6vw"}}}>
                        <Stack direction="row">
                            <Typography variant="h1" style={{fontSize: "2.5em", fontWeight: 300}}>Secure Checkout </Typography>
                            <HttpsIcon fontSize="large" sx={{marginTop: "0.15em", marginLeft: "0.3em"}}/>
                        </Stack>
                    </Box>
                    <Box sx={{marginLeft: {md: "7.6vw", sm: "5.2vw", xs: "2vw"}, marginTop: {lg: "2vh", xs: "1vh"}}}>
                        <Typography variant="body1" style={{fontSize: "1.5em",  fontWeight: 700}}>Enter your card information: </Typography>
                    </Box>

                    <PaymentForm pay={this.pay}/>

                    <Divider style={{marginTop: "7vh"}}/>
                    <Typography style={{fontSize: "1.3em", marginLeft: "6vw", fontWeight: 400, marginTop: "1vh"}}>Need some help? Call 1-800-MY-RAFAEL customer service 24/7</Typography>
                    <Divider style={{marginTop: "1vh"}}/>

                    <img src={LogoImage} alt={"Rafael Logo"} style={{marginTop: "5vh", maxWidth: '30vw', alignSelf: "center"}}/>
                </Stack>
            </>
        );
    }
}