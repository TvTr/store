import React from "react";
import {Alert, Box, Button, Divider, Stack} from "@mui/material";
import {Typography} from "@material-ui/core";
import LogoImage from '../../resources/logo.svg';
import AddressForm from "./AddressForm";
import ContactInformationForm from "../ContactInformationForm";
import PaymentsIcon from '@mui/icons-material/Payments';
import axios from "axios";
import {API} from "../../globals";
import {connect} from "react-redux";
import {mapStateToProps} from "../../redux/selectors";

class CheckoutFirstStage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            street: "",
            optional: "",
            zipcode: "",
            city: "",
            email: "",
            phoneNumber: "",
            firstNameError: false,
            lastNameError: false,
            streetError: false,
            zipcodeError: false,
            cityError: false,
            emailError: false,
            phoneError: false,
            isError: false,
            isErrorWithData: false,
            msg: ""
        }
        this.handleFormChange = this.handleFormChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.verifyData = this.verifyData.bind(this);
        this.handleSuccess = this.handleSuccess.bind(this);
        this.handleError = this.handleError.bind(this);
    }

    handleFormChange(e, property) {
        e.preventDefault();
        this.setState({
            [property]: e.target.value
        })
    }

    verifyData() {
        let verified = true;
        const phoneRegex = /^0(((\d{1}(-?)\d{8})|(\d{2}-\d{7}))|(\d{2}-\d{3}-\d{4}))$/;
        const emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        const changeState = (property, value) => {
            verified = verified ? !value : verified;
            this.setState({
                [`${property}Error`]: value
            })
        }
        if (!this.state.firstName.length)
        {
            changeState("firstName", true);
        }
        else
        {
            changeState("firstName", false);
        }
        if (!this.state.lastName.length)
        {
            changeState("lastName", true);

        }
        else
        {
            changeState("lastName", false);
        }
        if (!this.state.street.length)
        {
            changeState("street", true);
        }
        else
        {
            changeState("street", false);
        }
        if (isNaN(this.state.zipcode) || (this.state.zipcode.length !=5 && this.state.zipcode.length != 7))
        {
            changeState("zipcode", true);
        }
        else
        {
            changeState("zipcode", false);
        }
        if (!this.state.city.length)
        {
            changeState("city", true);
        }
        else
        {
            changeState("city", false);
        }
        if(!phoneRegex.test(this.state.phoneNumber))
        {
            changeState("phone", true);
        }
        else
        {
            changeState("phone", false);
        }
        if(!emailRegex.test(this.state.email))
        {
            changeState("email", true);
        }
        else
        {
            changeState("email", false);
        }

        return verified;
    }

    handleSubmit(e) {
        e.preventDefault();
        const getIdsOnly = (obj, fn) =>
                Object.entries(obj).map(
                    ([k, v], i) => {return {_id: k, amount: v.amount}}

            );

        if (this.verifyData())
        {
            const firstStage = {
                firstStage: {
                    products: getIdsOnly(this.props.products),
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    email: this.state.email,
                    phoneNumber: this.state.phoneNumber,
                    street: this.state.street,
                    optionalStreet: this.state.optional,
                    city: this.state.city,
                    zipcode: this.state.zipcode,
                }
            };
            axios.post(`${API}/orders/firstStage`, firstStage)
                .then(res => {this.handleSuccess(res)})
                .catch(res => {this.handleError(res)});
        }

    }

    handleSuccess(res) {
        if (res.data.error || !res.data._id)
        {
            this.setState({
                isErrorWithData: true,
                msg: res.data.params
            });
        }
        else
        {
            this.props.moveStage(res.data._id, res.data.price)
        }
    }

    handleError(res) {
        this.setState({
            isError: true,
            msg: res.data.payload
        });
    }

    render() {
        return (
            <>
                {this.state.isError ? <Alert severity="error" sx={{width: "80vw", transform: 'translateX(10%)', marginTop: "5vh"}}>Oops! An error with the server occurred, please try again later. {this.state.msg ? "[" + this.state.msg + "]" : ""}</Alert>
                    : <div></div>}
                {this.state.isErrorWithData ? <Alert severity="warning" sx={{width: "80vw", transform: 'translateX(10%)', marginTop: "5vh"}}>Oops! An error with the provided data occurred, please check your data and try again. {this.state.msg ? "[" + this.state.msg + "]" : ""}</Alert>
                    : <div></div>}
                <Stack direction="column"
                       sx={{ paddingBottom: "3vh", padding: "0.5em 0.5em 5vh 0.5em", marginTop: {sm: "3vh", md: "5vh", xs: "1.5vh"}}}
                       justifyContent="center">

                    <Box sx={{marginLeft: {xs: "2vw", sm: "5vw", md: "7.6vw"}}}>
                        <Typography variant="h1" style={{fontSize: "2.5em", fontWeight: 300}}>Where should we send your order? </Typography>
                    </Box>
                    <Box sx={{marginLeft: {md: "7.6vw", sm: "5.2vw", xs: "2vw"}, marginTop: {lg: "2vh", xs: "1vh"}}}>
                        <Typography variant="body1" style={{fontSize: "1.5em",  fontWeight: 700}}>Enter your name and address</Typography>
                    </Box>
                    <AddressForm state={this.state} onChange={this.handleFormChange}/>
                    <Divider variant="middle" sx={{marginTop:"2.5vh", marginLeft: {xs: "4vw", sm: "7vw", md: "8vw"}, width: {sm: "60vw", md: "41.5vw", xs: "85vw", lg: "31.5vw"}}}/>
                    <Box sx={{marginLeft: {md: "7.6vw", sm: "5.2vw", xs: "2vw"}, marginTop: "2vh"}}>
                        <Typography variant="body1" style={{fontSize: "1.5em", fontWeight: 700}}>What's your contact information?</Typography>
                    </Box>
                    <ContactInformationForm state={this.state} onChange={this.handleFormChange}/>
                    <Button variant="contained" sx={{marginLeft: {lg: "7vw", md: "7vw", sm: "4.5vw", xs: "1.7vw"}, marginTop: "3vh", width: {lg: "33.5vw", md: "43.8vw", sm: "66vw", xs: "90vw"}, minHeight: {xs: "5vh", sm: "4.5vh"}, borderRadius: "0.7em", textTransform: "none", fontSize: "1.2em"}} onClick={this.handleSubmit} startIcon={<PaymentsIcon/>}>Continue To Payment</Button>
                    <Divider style={{marginTop: "7vh"}}/>
                    <Typography style={{fontSize: "1.3em", marginLeft: "6vw", fontWeight: 400, marginTop: "1vh"}}>Need some help? Call 1-800-MY-RAFAEL customer service 24/7</Typography>
                    <Divider style={{marginTop: "1vh"}}/>

                    <img src={LogoImage} alt={"Rafael Logo"} style={{marginTop: "5vh", maxWidth: '30vw', alignSelf: "center"}}/>
                </Stack>
            </>
            );

    }
}

export default connect(mapStateToProps)(CheckoutFirstStage);