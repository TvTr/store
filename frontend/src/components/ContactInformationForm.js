import React from 'react';
import {Stack, TextField} from "@mui/material";

export default class ContactInformationForm extends React.Component {
    render() {
        return (
            <>
                <Stack direction="column" sx={{width: {md: "45vw", lg: "35vw", sm: "65vw", xs: "90vw"}, marginLeft: {md: "5.4vw", sm: "5vw", xs: "2vw"}}}>
                    <TextField sx={{marginTop: "1em", marginLeft: {md: "2.1vw"}}}
                               error={this.props.state.emailError}
                               required
                               value={this.props.state.email}
                               onChange={(e) => {this.props.onChange(e, "email")}}
                               id="outlined-required"
                               helperText={this.props.state.emailError ? "Enter a valid email address." : ""}
                               label="Email Address"
                    />
                    <TextField sx={{marginTop: "1em", marginLeft: {md: "2.1vw"}}}
                               error={this.props.state.phoneError}
                               required
                               value={this.props.state.phoneNumber}
                               onChange={(e) => {this.props.onChange(e, "phoneNumber")}}
                               id="outlined-required"
                               helperText={this.props.state.phoneError ? "Enter a valid phone number." : ""}
                               label="Phone Number"
                    />
                </Stack>

            </>
        )
    }
}