import * as React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {CardActionArea} from '@mui/material';
import {Link} from "react-router-dom";
import {Box} from "@material-ui/core";

export default function CollectionCard(props) {
    return (
        <Card sx={{maxHeight: {md: "45vh", lg: "50vh"}}}>
            <Link to={`/categories/${props.name}`} style={{textDecoration: "none", color: "inherit"}}>
                <CardActionArea>
                    <CardMedia>
                        <div>
                            <Typography gutterBottom variant="h5" component="div" style={{textAlign: "center"}}>
                                {props.name.toUpperCase()}
                            </Typography>
                        </div>
                        <Box sx={{maxWidth: {xs: "80vw", md: "30vw"}, height: {md: props.index > 2 ? "43vh" : "80%", lg: props.index > 2 ? "50vh" : "80%"}, paddingBottom: "2vh", alignContent: "center", alignSelf: "center"}}>
                                <img src={props.image} alt={props.name} style={{objectFit: "cover", width: '100%', verticalAlign: "center"}}/>
                        </Box>
                    </CardMedia>

                </CardActionArea>
            </Link>
        </Card>
    );
}