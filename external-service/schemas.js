const Joi = require("joi");

module.exports.paymentSchema = Joi.object({
    billingInfo: Joi.object({
        number: Joi.string().creditCard().required(),
        name: Joi.string().pattern(/^[a-zA-Z]+ [a-zA-Z]+$/i, 'full name'),
        cvc: Joi.string().pattern(/^[0-9]+$/, 'numbers').min(3).max(4).required(),
        expiry: Joi.date().min('now'),
        price: Joi.number().min(0)
    }).required()
})

module.exports.findTranscationSchema = Joi.object({
    billingInfo: Joi.object({
        lastName: Joi.string().pattern(/^[a-zA-Z]+$/i, 'last name').required(),
        price: Joi.number().min(0).required(),
        _id: Joi.string().pattern(/^[0-9a-f]{24}$/i).required(),
        lastDigits: Joi.string().pattern(/^[0-9]{4}$/i).required()
    }).required()
})