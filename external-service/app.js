const createError = require('http-errors');
const express = require('express');
const mongoose = require("mongoose");
const mongoSanitize = require('express-mongo-sanitize');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

mongoose.connect('mongodb://localhost:27017/rafael-transactions');
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error: "));
db.once("open", () => {
  console.log("Database connected");
});

const indexRouter = require('./routes/index');

const app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
    mongoSanitize({
      dryRun: true,
      onSanitize: ({ req, key }) => {
        console.warn(`[DryRun] This request[${key}] will be sanitized`, req);
      },
    }),
);

const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, id');
  next();
}

app.use(allowCrossDomain);
app.use('/', indexRouter);

app.all('*', (req, res, next) => {
    next(new ExpressError('Page Not Found :(', 404));
})

app.use((err, req, res, next) => {
    const {statusCode = 500} = err;
    if (!err.message) err.message = "Aww, man. Something went wrong!";
    res.status(statusCode).send({ err });
})

app.listen(9999, () => {
  console.log('Serving on port 9999');
});
