const express = require('express');
const router = express.Router();
const catchAsync = require("../utils/catchAsync");
const Transaction = require("../models/transaction");
const {paymentSchema, findTranscationSchema} = require("../schemas");

const LAST_DIGITS = 4;

const validatePayment = (req, res, next) => {
  const { error } = paymentSchema.validate(req.body);
  if (error) {
    const msg = error.details.map(el => el.message).join(',');
    console.log(msg);
    res.send({_id: null, err: true});
  } else {
    next();
  }
}

const validateFindTransaction = (req, res, next) => {
  const { error } = findTranscationSchema.validate(req.body);
  if (error) {
    const msg = error.details.map(el => el.message).join(',');
    console.log(msg);
    res.send({ans: false});
  } else {
    next();
  }
}

router.post('/', validatePayment, catchAsync(async function(req, res) {
  // making sure the data match the requirements
  const lastDigits = req.body.billingInfo.number.slice(req.body.billingInfo.number.length - LAST_DIGITS);
  console.log(req.body.billingInfo);
  /*
  some billing process to do
  */

  try {
    const transaction = await new Transaction({
      lastDigits: Number(lastDigits),
      firstName: req.body.billingInfo.name.split(" ")[0],
      lastName: req.body.billingInfo.name.split(" ")[1],
      price: req.body.billingInfo.price
    });

      transaction.save();

      if (transaction)
      {
        return res.send({_id: transaction._id, err: false});
      }
    } catch (e) {
      console.error(e);
    }
}));

router.post('/validate', validateFindTransaction, catchAsync(async function(req, res) {
    try {
      console.log(req.body.billingInfo);
      const transaction = await Transaction.findById(req.body.billingInfo._id);
      if (transaction)
      {
        console.log(transaction);
        if (transaction.lastDigits == req.body.billingInfo.lastDigits && transaction.lastName == req.body.billingInfo.lastName && transaction.price == req.body.billingInfo.price )
        {
          return res.send({ans: true});
        }
      }
    } catch (e) {
      console.log(e);
    }
  res.send({ans: false});
}));

module.exports = router;
